# cinc.gitlab.io

Sources of cinc.sh website.

## Requirements

You should have [hugo] installed. Please use the same version like used in the CI, please checkout the `.gitlab-ci.yml` for the used hugo version.

## How to work with it

```bash
# build the webpage and place it in the public folder
$ hugo
# run the local webserver with preview of website
$ hugo server
# create new blog post
$ hugo new blog/2019/10/title-of-post.md
# create a new signle static web page (e.g. about)
$ hugo new page/testpage.md
```

## Vendored foreign code

This repository contains hugo theme [minimo](https://github.com/MunifTanjim/minimo). This theme is vendored via git subtree in the folder themes/minimo and is licensed under MIT license.

Pulling of theme updates can be done like `git subtree pull --prefix themes/minimo https://github.com/MunifTanjim/minimo master`

[hugo]: https://gohugo.io
