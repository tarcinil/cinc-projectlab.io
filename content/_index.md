---
title: Home
menu: sidebar
weight: 1
---

## Cinc

A Free-as-in-Beer distribution of the open source software of Chef Software Inc. See [goals]({{< ref "goals.md" >}}) for details, or follow our [blog]({{< ref "blog/_index.md" >}}) for updates on the project.

The Cinc team is proud to present:

* [Cinc Client](http://downloads.cinc.sh/files/stable/cinc/), built from Chef Infra&trade;

Come chat with us! [community slack](http://community-slack.chef.io/) channel #community-distros

Contribute or join the team! [gitlab team](https://gitlab.com/cinc-project)

Visit other distribution projects! [Biome.sh](https://biome.sh/en/)

Have fun cooking with Cinc :)
