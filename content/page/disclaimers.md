---
comments: false
menu: sidebar
weight: 20
title: Disclaimers
---

Chef&reg;, Chef Infra&trade;, Chef InSpec&trade;, Chef Habitat&trade;, Chef Automate&trade;, Chef Workstation&trade;, Habitat&reg; and InSpec&reg; are trademarks of Chef Software Inc.

The Cinc project is in no way formally affiliated or associated with Chef Software Inc.

Other trademarks and trade names may be used in this document to refer to either the entities claiming the marks and/or names or their products and are the property of their respective owners. We disclaims proprietary interest in the marks and names of others.
