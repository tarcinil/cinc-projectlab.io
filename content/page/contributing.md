---
comments: false
menu: sidebar
title: Contributing
weight: 5
---

## Working upstream

Most of the work should be happening in Chef Software's repository. As such, anyone can contribute to Cinc's efforts without direct involvement in the project.

We have design documents on many upstream repositories detailing how we're implementing simpler redistribution under configurable names.

Anyone can read and open pull requests directly upstream to implement the design documents.

## Building pipelines

Our build and release processes should be 100% automated, that means pipelines! Join us on [Gitlab](https://gitlab.com/cinc-project/) and open an MR if you can improve on our pipelines.

## Feedback

Staying compliant with Chef Software's policy on trademarks requires vigilance from all users. If you see something user-facing that could violate the policy please open an issue in the relevant Cinc repository, or alternatively submit a fix directly upstream.

Similarly, we appreciate feedback on our packaging and distribution methods. If you find those inadequate let us know how we could improve them on Gitlab.
