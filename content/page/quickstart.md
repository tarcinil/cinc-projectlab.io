---
comments: false
title: QuickStart
menu: sidebar
weight: 14
---

## How to start using cinc

The Cinc family of tools is extremely similar to the Chef family of tools. If you don't know Chef, don't be afraid this document won't forget
you.

### What is the family?
- Cinc Client is the community version of Chef Infra Client
- Cinc Auditor is the community version of Chef Inspec
- [Biome](https://biome.sh) is the community version of Chef Habitat. Note that biome is done by another team from the [habitat community slack](http://slack.habitat.sh/) in #community-distros channel and not directly linked to the Cinc team's work. We'll have to work together to create biome packages of Cinc Client and Auditor.

### Learning the ropes

Cinc packages are community builds of Chef products, so the documentation still applies. The best resources for starting are https://learn.chef.io and https://docs.chef.io. While using those resources, you'll have to mentally replace `chef-client` with `cinc-client`, `inspec` with `cinc-auditor` and `hab` with `bio`

Small caveat: Cinc versions don't yet have biome packages. For the time-being you will have to stick with omnibus installation method of the programs for now.

### Install the package

All packages are published at http://downloads.cinc.sh. Download and install the packages as you would do for any other package on your system of
choice.

### Install using the install script

As an alternative to downloading packages directly, you can use the install script method described in [Chef's doc](https://docs.chef.io/install_omnibus.html) using https://omnitruck.cinc.sh/install.sh instead of the Chef install script.

For example, to install the latest 15.x version of Cinc Client on a supported UNIX/Linux/MacOS platform:
``` console
curl -L https://omnitruck.cinc.sh/install.sh | sudo bash -s -- -v 15
```

For a windows box:

``` powershell
. { iwr -useb https://omnitruck.chef.io/install.ps1 } | iex; install -version 15
```

### Configuration

Cinc Client gets it configuration from `/etc/cinc/client.rb` on UNIX/Linux/MacOS and `c:\cinc\client.rb` on Windows by default.

If you're migrating from Chef to Cinc, you can either use `cinc-client -c /etc/chef/client.rb` or replace the cinc default configuration file with
your Chef one: `cp /etc/chef/* /etc/cinc/`.

Advanced migration paths may need more work to rename/copy/suppress one of the directory and create a symbolic link between the
two directories.

## You're Good to Go :)

If you have questions, feel free to join us in the #community-distros channel on Chef's [community
slack](http://community-slack.chef.io/)
