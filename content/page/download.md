---
comments: false
title: Download
menu: sidebar
weight: 10
---

[Our packages are here](http://downloads.cinc.sh)

Cinc-client can be programmatically installed using https://omnitruck.cinc.sh/install.sh as you would do for an [omnibus install of Chef](https://docs.chef.io/install_omnibus.html)

## Trademark compliance status

As of 2020/01/20, the project status goes as follows:

| Product          | Status      | Deemed compliant | Evaluated by Chef  |
|------------------|-------------|------------------|--------------------|
| Client (Linux)   | Approved    | >= 15.6.10       | December 4th, 2019 |
| Client (Windows) | In review   | N/A              | N/A                |
| Auditor          | In review   | N/A              | N/A                |
| Workstation      | In progress | N/A              | N/A                |
| Server           | See below   | N/A              | N/A                |
| Packager         | See below   | N/A              | N/A                |
| Dashboard        | Not started | N/A              | N/A                |

If you are using Cinc binaries, we strongly recommend updating to an approved version as they become available.

Looking for Cinc Server? It's not ready, but do try this alternative: https://github.com/ctdk/goiardi

Hoping to get your hands on Packager? Not ready either, but the Biome project has you covered in the meantime: https://biome.sh
