---
comments: false
menu: sidebar
title: Goals
weight: 4
---

The Cinc projects defines two goals for itself.

## Making Chef Software Inc's open source products easily distributable, by anyone

We're not just building a distribution, we are making the software distributable! We are doing so by submitting changes directly upstream.

These changes are designed to make it simple for anyone to create their own distribution of these products. As an example, in https://github.com/chef/chef we have created a file called `dist.rb` which contains constants that map to all of Chef Software's trademarks. Distributors can now patch or replace only 3 files, namely `dist.rb`, the `gemspec` and the `omnibus` configuration, to produce their own distributions.

See how we do our own builds here: https://gitlab.com/cinc-project/client/tree/master

## Creating free distributions of Chef Software Inc's open source products

We aim to provide end-users with the same value and experience they've come to expect from these tools over the years.

We're building directly from Chef Software's github repos, applying a simple patch as described above, and distributing the resulting binaries.

**This is not a fork, all functionalities are identical**. Most differences are purely cosmetic and intended solely to comply with the [Chef Software Trademark policy](https://www.chef.io/trademark-policy/).

We currently distribute Cinc Client, which has been thoroughly evaluated by Chef Software for compliance with the [Chef Software Trademark policy](https://www.chef.io/trademark-policy/). 

**All Cinc binaries are licensed under [Apache2.0](https://www.apache.org/licenses/LICENSE-2.0) and are free of any further EULAs or licensing.**

The Cinc team makes no guarantee of compliance with the trademark policy and is working on a best effort basis. That said, we're receiving guidance directly from Chef Software and they have been very cooperative in this endeavor.

In the future, we would also like to distribute:

- Cinc Workstation, a free distribution of Chef Workstation&trade;
- Cinc Auditor, a free distribution of Chef Inspec&trade; (currently [available]({{< ref "download.md" >}}) for testing
- Cinc Dashboard, a free distribution of Chef Automate&trade;
- Cinc Packager, a free distribution of Chef Habitat&trade;

### What about other ecosystem tools?

The Cinc project's involvement will be limited to redistributing binaries produced by Chef Software Inc.

For example, this would include a free distribution of Chef Infra Server, but not of test-kitchen, as the latter is a 3rd party project.

We encourage anyone concerned about 3rd party tools to contact the maintainers of those projects directly.

We will however make reasonable efforts to ensure that Cinc is compatible with those 3rd party tools.
