---
robotsdisallow: false
comments: false
title: About
menu: sidebar
weight: 15
---

## What's with the name ?
Cinc is a recursive acronym for **C**INC **I**s **N**ot **C**hef

## Who's involved in this ?
We're a small group of people trusting a free distribution of Chef is necessary alongside the commercial one made by Chef. To address Chef Software Inc's [trademark policy](https://www.chef.io/trademark-policy/), we worked in collaboration with them on creating compliant builds of their OSS products.

The Cinc project is an open community, and we welcome everyone to contribute however they are able! There are many ways to do so, see [contributing]({{< ref "Contributing.md" >}}) for details.

You can see the team in Gitlab's [Cinc project team tab](https://gitlab.com/groups/cinc-project/-/group_members)

The most active contributors are: (name/nickname: primary involvement):

- Artem Sidorenko (known only as himself!): Top contributor to Cinc auditor.
- Lance Albertson AKA Ramereth: Providing hosting via the [OSUOSL](https://osuosl.org/) and pipeline definitions.
- Marc Chamberland AKA BobChaos: Top contributor to Cinc Client and Workstation 
- Tensibai: Pipeline definitions and initial hosting provider.

## Is Cinc compatible with upstream products ?
Yes, it's the same code as the original products, only branding is changed.

Cinc provides the exact same value to end-users as the upstream products. Your Chef Infra&trade; cookbooks will work just as well in Cinc Client.

Transitioning from a Chef Software product to a Cinc project one or vice-versa should be simple and seamless.

## How does CINC ensure compliance with Chef Software’s trademark policy or the quality of the software?
Cinc products are distributed with no formal warranties or support of any kind.

However, we have actively worked with Chef Software Inc. and the Chef community to ensure that we have - to the best of our ability - met [Chef's trademark and policy requirements](https://www.chef.io/trademark-policy/).

Cinc products are built from the exact same code as Chef Software Inc's products, substituting only a few constants. You can reasonably expect both compliance and quality with every release of Cinc products.

If warranties and support are things that you _need_, the Cinc project recommends contacting Chef Software Inc directly.

Additionally, while we do not officially offer support you'll find we're helpful people, feel free to ask for help!

## Cinc and licensing
The Cinc project's stated goal is to provide 100% free distributions of Chef Software Inc's products. All our binaries are distributed under [Apache2.0](https://www.apache.org/licenses/LICENSE-2.0) now, and for as long as the Cinc projects will produce them.

See [goals]({{< ref "goals.md" >}}) for more details on our mission statement.

## Special thanks to

* ([OSUOSL](https://osuosl.org)) which provides the Gitlab CI runners for Linux, Windows and Mac OS builds.
* [Chef Software Inc](https://www.chef.io/), which authored and maintains the software and has provided guidance to the Cinc project.
* [The Biome team](https://biome.sh/en/), who provides both it's software distribution and advice on how we can improve ours.
* [The Chef Community](https://www.chef.io/community/) for your support and feedback
* Jeremy Bingham AKA ct, for his [Goiardi](http://goiardi.gl/), support and feedback
