---
date: "2019-12-04T16:05:34-05:00"
title: "Cinc Client is live"
authors: ["bobchaos"]
tags:
  - Cinc
  - Client
  - Release
  - Status
---

## The Cinc project is proud to announce the initial release of Cinc Client

It's been a long time coming, but the Cinc team is finally ready to announce it's first official release of Cinc Client for Linux. While we have been distributing it for some time, it came with all kinds of disclaimers about compliance with Chef Software's policy on trademarks. No longer!

Chef Software has reviewed Cinc Client and deemed it compliant with it's policy as reported on Chef's Community Slack. Sadly it's not a paid Slack, so it will eventually vanish in the backlog, so we took a nice screenshot for posterity: https://gitlab.com/cinc-project/client/issues/10

A big thanks to the folks at Chef Software for taking the time to review our builds so we can publish them with confidence.

## GimmeGimme!

Checkout our [quickstart page]({{< ref "quickstart.md" >}}) for some basic instructions, or just jump right in by using our omnitruck implementation `curl -L https://omnitruck.cinc.sh/install.sh | sudo bash`

Prefer plain old packages? We got you covered too! Checkout our [download]({{< ref "download.md" >}}) page of skip right ahead to the [latest releases of Cinc Client!](http://downloads.cinc.sh/files/stable/cinc/)

Test-kitchen is where you like to try out your new toys? Put this in your kitchen file's `provisioner` section:

```
provisioner:
  name: chef_zero
  require_chef_omnibus: true
  chef_omnibus_url: https://omnitruck.cinc.sh/install.sh
  chef_omnibus_root: /opt/cinc
```

We do not have hart packages available at this time, but it's only a matter of time. If you're in a hurry, as always we welcome [contributions]({{< ref "contributing.md" >}}).

### What next?

Work is already well underway to producing a fully compliant Cinc Client for Windows platforms, and we expect a first official release in the near future. In the meantime feel free to [download]({{< ref "download.md" >}}) our test builds and provide feedback through [the usual channels]({{< ref "contributing.md" >}}).

Cinc Auditor is also ready for evaluation and the Cinc project has requested that Chef Software take a look at it too. We already distribute test builds so we can gather feedback from the community, but we do not recommend using them for any commercial purposes at this time.

Stay tuned to this blog to find out when more binaries get evaluated!
